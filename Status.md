# Status

## Dec 4, 2023, later in the day
Major progress on the BigDb package. Need to test the migrations. Need to setup the easy orm & easy db. need to add sample code to the sample bigdb, bigdb cli & bigorm.

## Dec 4, 2023
Notes:
- `sample/` contains files to-be-copied to a new project
- `sample/` also may contain some things needed by this repo internally (like the Cli class, which is both how we setup a new project AND is a sample file for a new cli).
- `src/` contains subclasses for easier use of some of my tools (like a cli subclass)
- `src/` also contains things needed by this repo internally (like packages.json)
- `temp/` is a test playground for running the tlf bin script & seeing if it works
- `docsrc/*` & `doctemplate/*` double as sample files AND files actually used by this repo.
- `config/*` doubles as sample files AND files used by this repo
- `bin/*` doubles as sample files and files used by this repo (particular `tlf` script)

Goals:
- Add packages to `src/packages.json` for all the different packages in this repo
- Add functional sample tests for every package.
- Document this repo's main functionality of ... setting up new projects.
- Document this repo's convenience classes - It's not HIGHLY needed bc we copy+paste sample classes, but it would be a nice plus.


## Dec 3, 2023
Created repo & scaffolded a lot.

Projects I want to add to this repo:
- json to mysql (a cli command, maybe a setup that downloads a json file then converts to mysql to regenerate a database.
- Markdown to PDF (probably just a cli command to run the generation)
- pHtml - Just some sample parsing & traversing code (also, i need to fix where it breaks when a php tag is not closed)
- Resources (json file & php setup in cli & deliver)
- Spreadsheet to db (cli command & maybe similar to jsontomysql ideas)
- Autowire.js

Liaison Apps to add to this repo:
- Seo Extractor
- User Login
- Project Viewer (NOT soon though bc the project viewer is pretty sketchy)
- Files (except its packaged as part of Phad right now)

