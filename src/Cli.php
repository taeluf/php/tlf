<?php

namespace Tlf\Easy;

class Cli extends \Tlf\Cli {

    protected array $commands = [];

    public function init(){

        $call = [$this, 'exec_command'];

        if (!isset($this->commands['main'])){
            $this->load_command('main',
                function($cli, $args){
                    $cli->call_command('help',[]);
                }, "show this help menu"
            );
        }

        foreach ($this->commands as $command => $help_message){
            $this->load_command($command,$call,$help_message);
        }

    }
    
    /**
     * Execute the currently called command
     *
     * @param $cli The cli object being called by the user.
     * @param $args array of args, with $args['--'] containing unnamed args.
     */
    public function exec_command(\Tlf\Cli $cli, array $args){
        $command = $this->command;
        $unnamed_args = $args['--'] ?? [];
        unset($args['--']);

        $func = 'cmd_'.str_replace('-','_',$command);
        if (!method_exists($this, $func)){
            echo "\nCommand '$command' does not exist\n";
            return;
        }

        $this->$func($unnamed_args, $args);
    }
}
