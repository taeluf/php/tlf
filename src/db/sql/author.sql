

-- @query(create, -- END)  
DROP TABLE IF EXISTS `author`;  
CREATE TABLE `author` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT (UUID_TO_BIN( UUID() ) ),

  `name` varchar(256) NOT NULL,
  `slug` varchar(256) NOT NULL,
  `about` TEXT NOT NULL,

  `photo_id` int unsigned, 
  `user_id` int unsigned NOT NULL,

  `created_at` DATETIME DEFAULT NOW(),
  `updated_at` DATETIME DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  UNIQUE(`uuid`),
  UNIQUE(`slug`), 

  FOREIGN KEY (`user_id`) REFERENCES `user`(`id`),
  FOREIGN KEY (`photo_id`) REFERENCES `file`(`id`) ON DELETE SET NULL
); -- END
