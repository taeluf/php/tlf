
-- @query(create, -- END)  
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `article`;  
CREATE TABLE `article` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT (UUID_TO_BIN( UUID() ) ),

  `title` varchar(256) NOT NULL,
  -- `relation_type` ENUM('none', 'correction') NOT NULL DEFAULT 'none',
  `type` ENUM('original-report', 'meta-report', 'summary', 'opinion', 'fact-check', 'blog') NOT NULL,
  `slug` varchar(256) NOT NULL,
  `story_slug` varchar(256) DEFAULT NULL,

  `photo_id` int unsigned DEFAULT NULL, -- for a cover photo
  `story_id` int unsigned DEFAULT NULL,
  `author_id` int unsigned NOT NULL, 

  `tags` varchar(512) DEFAULT '',
  `status` ENUM('public','private','archive','draft') DEFAULT 'public',
  `show_history` BOOLEAN DEFAULT true,

  `created_at` DATETIME DEFAULT NOW(),
  `updated_at` DATETIME DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  UNIQUE(`uuid`),
  UNIQUE(`story_slug`, `slug`),
  FOREIGN KEY (`author_id`) REFERENCES `author`(`id`)
);
-- END

-- @query(get_by_uuid)
SELECT * FROM `article` WHERE `uuid` LIKE :uuid;
