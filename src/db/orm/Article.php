<?php

namespace Tlf\ProjectSample;

/**
 * Represents an article in the database.
 *
 */
class Article extends \Tlf\Easy\BigOrm {

    /**
     * @var \DecaturVote\News\Db\BigNewsDb
     */
    public \Tlf\BigDb $db;

    // old properties
    //protected string $mdBody;
        //protected string $htmlBody;
        //protected Story $story;
        /** Placeholder to force getTags() setter. */
        //protected $tags = null;
        //protected array $tagged_by;
    // end old properties

    // DB properties
    public ?int $id = null;
    public ?string $uuid = null; 

    public string $title;
    /** should match [a-z0-9\-] */
    public string $slug;

    public ?int $photo_id = null;
    public ?int $story_id = null;
    public int $author_id;
    
    protected string $tags = ''; // simple string of tags as stored in database. 
    public \DecaturVote\News\Types\ArticleStatus $status = \DecaturVote\News\Types\ArticleStatus::Public;
    public \DecaturVote\News\Types\ArticleType $type;
    public bool $show_history = true;

    public \DateTime $created_at; 
    public \DateTime $updated_at;


    public ?string $story_slug = null;
    public ?string $story_title = null; 

    // end DB properties

    // filesystem properties
    /** html article body */
    protected string $body = '';
    // end filesystem properties
    
    // derived properties
    protected string $url;
    // end derived properties

    // relationships, not stored in the DB row
    protected \Dv\File\Db\File $photo;
    protected ?Story $story; 
    /** array<int index, DecaturVote\News\Orm\Tidbit tidbit> array of tidbits referencing this article */
    protected ?array $tidbits;
    /** array<int index, DecaturVote\News\Orm\Source source> array of sources referencing this article */
    protected ?array $sources;
    protected Author $author;
    protected array $tag_list = [];


    protected ?string $photoUrl = null;
    // end relationships

    public function __construct(\DecaturVote\News\Db\BigNewsDb $db){
        parent::__construct($db);
        $this->created_at = new \DateTime('now', $db->integration->getTimezone());
        $this->updated_at = new \DateTime('now', $db->integration->getTimezone());
    }
    
    public function set_from_db(array $row){
        $this->row_to_props($row, 'created_at', 'updated_at', 'status', 'type', 'uuid');
    }

    public function get_db_row(): array {
        $row = $this->props_to_row(
            ['id', 'title', 'slug', 'story_slug', 'photo_id', 'story_id', 'author_id', 'tags', 'show_history'],
            ['created_at', 'updated_at', 'status', 'type', 'uuid'],
        );
        if ($row['uuid'] == null) unset($row['uuid']);

        return $row;
    }

    public function set_from_form(array $data, mixed $form_id = null){
        $this->tag_list = [];
        unset($data['tag_name']); // input used to lookup tags in GUI. Tag IDs are submitted under 'tag' array, for db storage.
        if (!isset($data['tag']))$data['tag'] = [];
        // VALIDATE PROPERTIES
        $is_valid = $this->validate_data($data, 'form/article');

        if (!$is_valid){
            throw new \Exception("Submitted data is not valid");
        }

        // DERIVE PROPERTIES / modify data
        $body = $data['html_body'];
        unset($data['html_body']);

        //if (!isset($data['slug']))$data['slug'] = $this->slugify($data['title']);

        // convert array of tag ids to a name:uuid string
        $query_in = [];
        foreach ($data['tag'] as $tag_id){
            $to_test = (string)((int)$tag_id);
            if ($tag_id!==$to_test)continue;
            $query_in[] = (int)$tag_id;
        }
        unset($data['tag']);
        $search_db = new \DecaturVote\SearchDb($this->db->getPdo());
        $tag_list = $search_db->get_tags_in($query_in);
        $tags_string = [];
        foreach ($tag_list as $tag){
            $tags_string[] = $tag->tag_name.':'.$tag->tag_uuid;
        }
        $data['tags'] = implode(',', $tags_string);

        $this->author_id = $this->get_current_author_id();

        if (is_string($data['story_id'])){
            if ($data['story_id']=='')unset($data['story_id']);
            else $data['story_id'] = (int)$data['story_id'];
        }
        if (is_string($data['photo_id'])){
            if ($data['photo_id']=='')unset($data['photo_id']);
            else $data['photo_id'] = (int)$data['photo_id'];
        }

        // SET PROPERTIES
        $this->set_props_from_array($data, 'tags', 'title', 'slug', 'photo_id', 'story_id', 'show_history');

        $this->body = $body;

        if (!isset($this->created_at)){
            $this->created_at = new \DateTime('now', $this->db->integration->getTimezone());
        }
        $this->updated_at = new \DateTime('now', $this->db->integration->getTimezone());

        $this->status = \DecaturVote\News\Types\ArticleStatus::from($data['status']);
        $this->type = \DecaturVote\News\Types\ArticleType::from($data['type']);
    }

    /** 
     * Prepare for save:
     * - Set story_title & story_slug if story_id is set
     * - Set own slug (if not already set)
     * 
     * Notes:
     * - does NOT set author_id, that's done during set_from_form. If not using set_from_form, it's the programmer's responsibility to determine & set author_id.
     *
     * @return modified / saveable row
     */
    public function onWillSave(array $row): array {
        if (!isset($this->slug))$this->slug = $this->slugify($this->title);
        if (isset($this->story_id)){
            $story_list = $this->db->get('story', ['id'=>$this->story_id]);
            if (count($story_list)==0)$this->story_id = null;
            else {
                $story = $story_list[0];
                $this->story_slug = $story->slug;
                $this->story_title = $story->title;
            }
        } else {
            $this->story_slug = null;
            $this->story_title = null;
        }


        return $this->get_db_row();
    }
    /** 
     * Set the UUID, store body to disk, run search integration, run diff integration
     * TODO: Run mentions integration
     */
    public function onDidSave(array $row) {
        // set uuid
        if (!isset($this->uuid)){
            $data = $this->db->sql_query("SELECT uuid FROM article WHERE id=:id", ['id'=>$this->id]);
            $this->uuid = $this->bin_to_uuid($data[0]['uuid']);
        }

        // store new body to disk (if changed)
        $current_body = $this->getBody();
        $path = $this->db->get_article_file_path($this->uuid);
        $old_body = '';
        if (file_exists($path))$old_body = file_get_contents($path);
        if ($current_body!=$old_body){
            if (!file_exists(dirname($path)))mkdir(dirname($path), 0751);
            file_put_contents($path, $current_body);
        }

        // create and store diffs
        $diffDb = new \DecaturVote\DiffDb($this->db->getPdo());
        $diffDb->update_permissions($this->uuid, $this->status->value);
        if ($current_body != $old_body){
            $diffDb->store_diff($old_body, $current_body, $this->uuid, $this->status->value, 'table:article');
        }

        // integrate with search
        $this->update_search();

    }
//public function onWillDelete(array $row): bool: Hook called before an item is deleted. Return false to stop deletion, true to continue
    public function onDidDelete(array $row) {
        $this->status = \DecaturVote\News\Types\ArticleStatus::Deleted;
        $path = $this->db->get_article_file_path($this->uuid);
        if (file_exists($path))unlink($path);

        $this->update_search();

        $diffDb = new \DecaturVote\DiffDb($this->db->getPdo());
        $diffDb->update_permissions($this->uuid, $this->status->value);

    }


    /** 
     * Get array of tags. Lazy-loads from db.
     *
     * @return array <int index, DecaturVote\SearchDb\Tag tag> array of tag objects
     */
    public function getTags(): array {
        if (count($this->tag_list)>0)return $this->tag_list;
        if (strlen($this->tags)==0)return [];

        $tag_list = explode(',',$this->tags);

        // NOTE: We could just query the tagged_by table
        $uuids = [];
        foreach ($tag_list as $index => $tag_definition){
            $parts = explode(':',$tag_definition);
            if (!isset($parts[1]))continue;
            $uuid = $parts[1];
            $uuids[] = $uuid;
        }

        $db = new \DecaturVote\SearchDb($this->db->getPdo());
        //$tag_list = $db->get_tags_by_uuids($uuids);
        //var_dump($tag_list);
        $this->tag_list = $db->get_tags_by_uuids($uuids);

        return $this->tag_list;
    }

    /** @deprecate use getBody() instead */
    public function getHtmlBody(){return $this->getBody();}
    /**
     * Get the article body. Lazy loads from disk.
     *
     * @return string html article body.
     */
    public function getBody(): string {
        if (isset($this->body) && $this->body != '')return $this->body;
        if (!isset($this->uuid)){
            $this->body = '';
            return '';
        }
        $file = $this->db->get_article_file_path($this->uuid);
        if (!file_exists($file)){
            $this->body = '';
            return '';
        }
        $body = file_get_contents($file);
        $this->body = $body;
        return $body;
    }

    public function setBody(string $body){
        $this->body = $body;
    }


    public function getUrl(){
        if (empty($this->story_slug))return '/article/'.$this->slug.'/';

        return '/story/'.$this->story_slug.'/'.$this->slug.'/';
    }

    /** 
     * Set page breadcrumbs for the article, by invoking the News integration.
     *
     */
    public function setBreadcrumbs(){
        if ($this->story_id!=null){
            $story = $this->getStoryUrl();
            $this->db->integration->setBreadcrumbs(['/'=>'Home', '/stories/'=>'Topics', $this->getStoryUrl()=>$this->getStoryTitle()]);
            return;
        }  
        
        $this->db->integration->setBreadcrumbs(['/'=>'Home', '/articles/'=>'Articles']);
    }


    public function getStory(): ?\DecaturVote\News\Orm\Story {
        if ($this->story_id==null)return null;
        if (isset($this->story))return $this->story;
        $this->story = $this->db->get('story', ['id'=>$this->story_id])[0] ?? null;
        return $this->story;
    }


    /**
     * Get an array of tidbits
     * @return array<int index, DecaturVote\News\Orm\Tidbit tidbit> array of tidbit objects attached to this article.
     */
    public function getTidbits(): ?array {
        if (isset($this->tidbits) && is_array($this->tidbits))return $this->tidbits;
        else if (isset($this->tidbits))return [];
        else if (!$this->is_saved())return [];

        $this->tidbits = $this->db->get('tidbit', ['article_id' => $this->id]);

        return $this->tidbits;
    }

    /**
     * Get an array of tidbits
     * @return array<int index, DecaturVote\News\Orm\Source source> array of source objects attached to this article.
     */
    public function getSources(): ?array {
        if (isset($this->sources) && is_array($this->sources))return $this->sources;
        else if (isset($this->sources))return [];
        else if (!$this->is_saved())return [];

        $this->sources = $this->db->get('source', ['article_id' => $this->id]);

        return $this->sources;
    }

    /**
     * Get url of the story this article is about
     * @return string url (no domain or https) or null
     */
    public function getStoryUrl(): ?string {
        if (is_null($this->story_slug))return null;
        return '/story/'.$this->story_slug;
    }

    /**
     * Get title of the story this article is about
     * @return string title or null
     */
    public function getStoryTitle(): ?string {
        if (is_null($this->story_title))return null;
        return '/story/'.$this->story_title;
    }


    public function getEditUrl(){
        return '/article/create/?id='.$this->id;
    }

    /**
     * Get a human friendly type string.
     * @deprecate use $article->type->human_friendly() instead
     * @return string human-friendly like 'Original Report' or 'Fact Check'
     */
    public function getReadableType(){
        return $this->type->human_friendly();
    }

    /**
     * Get a url to the type's description
     * @deprecate use $article->type->anchor_tag() instead
     * @return string of an anchor tag
     */
    public function getTypeLink(): string {
        return $this->type->anchor_tag();
    }


    /**
     * Add this article to the search database ONLY IF its status is public. Delete it from search if not public.
     *
     * @param $pdo the pdo instance to use for database operations.
     * @param $tags array<int index, int tag-id>
     *
     * @return true if saved to search. false otherwise
     */
    public function update_search(): bool {
        $pdo = $this->db->getPdo();
        // add to search
        $searchDb = new \DecaturVote\SearchDb($pdo);
        // we re-add all tags on every save (if public), so we first delete them all.
        $searchDb->delete_tagged_by($this->uuid);

        $searchDb->delete_from_search($this->uuid);
        $searchDb->delete_from_tags($this->uuid);

        if (!$this->is_saved() || $this->status != \DecaturVote\News\Types\ArticleStatus::Public){
            return false;
        } 

        $searchable = $searchDb->add_searchable(
            $this->uuid, 
            'article', 
            $this->title, 
            strip_tags($this->getBody()), 
            '/mention/article/'.$this->uuid.'/', 
            $this->updated_at,
        );

        $tag_list = explode(',',$this->tags);

        foreach ($tag_list as $index => $tag_definition){
            $parts = explode(':',$tag_definition);
            if (!isset($parts[1]))continue;
            $uuid = $parts[1];
            $searchable->add_tag_by_uuid($uuid);
        }

        return true;
    }


}
