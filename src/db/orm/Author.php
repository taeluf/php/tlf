<?php

namespace Tlf\ProjectSample;

class Author extends \Tlf\Easy\BigOrm {

    public ?int $id = null;
    public ?string $uuid = null; 

    public string $name;
    /** should match [a-z0-9\-] */
    public string $slug;
    public string $about;

    public ?int $photo_id = null;
    public ?int $user_id = null;

    public \DateTime $created_at; 
    public \DateTime $updated_at;


    protected \Dv\File\Db\File $photo;
    //public $user;

    /**
     * set during onWillSave() & used during onDidSave() to handle diffs updates
     */
    protected string $previous_about_section = '';

    public function getUrl(){
        return '/author/'.$this->slug.'/';
    }



    public function __construct(\DecaturVote\News\Db\BigNewsDb $db){
        parent::__construct($db);
        $this->created_at = new \DateTime('now', $db->integration->getTimezone());
        $this->updated_at = new \DateTime('now', $db->integration->getTimezone());
    }
    
    public function set_from_db(array $row){
        $this->row_to_props($row, 'created_at', 'updated_at', 'uuid');
    }

    public function get_db_row(): array {
        $row = $this->props_to_row(
            ['id', 'name', 'slug', 'about', 'photo_id', 'user_id'],
            ['created_at', 'updated_at', 'uuid'],
        );

        // so that mysql will generate the uuid
        if ($row['uuid']==null)unset($row['uuid']);

        return $row;
    }

    public function set_from_form(array $data, mixed $form_id = null){
        // VALIDATE PROPERTIES
        $is_valid = $this->validate_data($data, 'form/author');

        if (!$is_valid){
            throw new \Exception("Submitted data is not valid");
        }

        if (!isset($this->user_id)){
            // @TODO should I block submission if current user id does not match? Probably, unless current user is admin...
            $this->user_id = $this->db->integration->get_user_id();
        }

        if (is_string($data['photo_id'])){
            if ($data['photo_id']=='')unset($data['photo_id']);
            else $data['photo_id'] = (int)$data['photo_id'];
        }

        // SET PROPERTIES
        $this->set_props_from_array($data, 'name', 'about', 'slug', 'photo_id');

        if (!isset($this->created_at)){
            $this->created_at = new \DateTime('now', $this->db->integration->getTimezone());
        }
        $this->updated_at = new \DateTime('now', $this->db->integration->getTimezone());

    }

    /** 
     * Prepare for save:
     * - Set story_title & story_slug if story_id is set
     * - Set own slug (if not already set)
     * 
     * Notes:
     * - does NOT set author_id, that's done during set_from_form. If not using set_from_form, it's the programmer's responsibility to determine & set author_id.
     *
     * @return modified / saveable row
     */
    public function onWillSave(array $row): array {

        if (!$this->is_saved()){
            $current_user_id = $this->db->integration->get_user_id();
            $existing_author_list = 
                $this->db->sql_query(
                    "SELECT id FROM author WHERE user_id = :current_user_id",
                    ['current_user_id' => $current_user_id]
                );
            if (count($existing_author_list) > 0){
                throw new \Exception("A new author page cannot be created because the logged in user with id '$current_user_id' already has an author page under the author id: ".print_r($existing_author_list, true)."\nThey must edit their existing page, or delete their page THEN create a new one");
            }
        }

        if (!isset($this->slug))$this->slug = $this->slugify($this->name);

        if ($this->is_saved()){
            $data = $this->db->sql_query("SELECT `about` FROM author WHERE id=:id", ['id'=>$this->id])[0];
            $pre_existing_body = $data['about'];
        } else $pre_existing_body = '';

        $this->previous_about_section = $pre_existing_body;

        return $this->get_db_row();

    }
    /** 
     * Set the UUID, store body to disk, run search integration, run diff integration
     * TODO: Run mentions integration
     */
    public function onDidSave(array $row) {
        // set uuid
        if (!isset($this->uuid)){
            $data = $this->db->sql_query("SELECT BIN_TO_UUID(uuid) as uuid FROM author WHERE id=:id", ['id'=>$this->id]);
            $this->uuid = $data[0]['uuid'];
        }

        $this->update_search();

        // create and store diffs
        $diffDb = new \DecaturVote\DiffDb($this->db->getPdo());
        //$diffDb->update_permissions($this->uuid, $this->status->value);
        if ($this->about != $this->previous_about_section){
            $diffDb->store_diff($this->previous_about_section, $this->about, $this->uuid, 'unknown-permissions', 'table:author');
        }

    }
//public function onWillDelete(array $row): bool: Hook called before an item is deleted. Return false to stop deletion, true to continue
    public function onDidDelete(array $row) {
        //$this->status = \DecaturVote\News\Types\ArticleStatus::Deleted;

        $this->update_search();

        $diffDb = new \DecaturVote\DiffDb($this->db->getPdo());
        $diffDb->update_permissions($this->uuid, 'deleted');

    }




    /**
     * Add this author to the search database
     *
     * @return true if saved to search. false otherwise
     */
    public function update_search(): bool {
        $pdo = $this->db->getPdo();
        // add to search
        $searchDb = new \DecaturVote\SearchDb($pdo);

        if (!$this->is_saved()){
            $searchDb->delete_from_search($this->uuid);
            return false;
        }
        //if (!$this->is_saved() || $this->status != \DecaturVote\News\Types\ArticleStatus::Public){
            //return false;
        //} 

        $searchable = $searchDb->add_searchable(
            $this->uuid, 
            'author', 
            $this->name, 
            strip_tags($this->about), 
            '/mention/author/'.$this->uuid.'/', 
            $this->updated_at,
        );

        return true;
    }
}
