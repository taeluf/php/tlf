<?php

///////
//
/// SERVER test routes
//
///////
$url = $_SERVER['REQUEST_URI'];
$parsed = parse_url($url);
$url = $parsed['path'];
if ($url=='/redirect/'){
    header("Location: /finished-redirect/");
    echo 'zeep';
    exit;
} else if ($url=='/finished-redirect/'){
    echo 'successful redirect';
} else if ($url=='/file-upload/'){

    echo 'File Content('.$_FILES['test_txt']['type'].'):'
        .file_get_contents($_FILES['test_txt']['tmp_name']);
} else if ($url=='/request-method/'){
    echo $_SERVER['REQUEST_METHOD'];
} else if ($url=='/') {
    echo "server-test";
}



/////////
//
//// BROWSER test routes
//
/////////
if ($url=='/browser/'){
    echo 'browser-test';
} else if ($url=='/go-home/'){
    header("Location: /browser/");
    exit;
} else if ($url=='/param/'){
    echo $_GET['param'];
} else if ($url=='/post/'){
    echo 'postme:'.$_POST['postme'];
} else if ($url=='/file/'){
    $file = $_FILES['file'];
    echo $file['name'].':'.file_get_contents($file['tmp_name']);
}
// else {
    // echo "failed at '$url'";
// }
