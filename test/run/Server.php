<?php

namespace Tlf\Tester\Test;

class Server extends \Tlf\Tester {

    public function testMultiServer1(){
        $response = $this->get('/',[], '1');

        $this->compare(
            $response,
            "this is server 1"
        );
    }

    public function testMultiServer2(){
        $response = $this->get('/',[], '2');

        $this->compare(
            $response,
            "this is server 2"
        );

    }

    public function testRequestMethod(){
        $response = $this->get('/request-method/');
        $this->compare('GET', $response);


        $response = $this->post('/request-method/');
        $this->compare('POST', $response);
    }

    public function testFileUpload(){
        $response = $this->post('/file-upload/',[], 
            [ 'test.txt' => $this->file('test/input/test-upload.txt') ]
        );

        echo $response;
        $this->compare('File Content(text/plain):this file is for testing uploads', $response);
    }

    public function testPostRedirect(){
        $response = $this->post('/redirect/');

        $this->compare('successful redirect', $response);
    }

    /**
     * @test that the phptest server works
     */
    public function testServer(){
        $response = $this->get('/');
        $this->compare('server-test', $response);
    }
}

