<?php

namespace Tlf\Tester\Test;

class Compare extends \Tlf\Tester {

    public function testCompareLines(){

        $this->compare_lines(
            "abc\ndef\nghi\n\n\n    jkl  \nmnop  \n\n",
            "  abc  \n def \n \n ghi \n\n\n jkl  \n\n mnop"
        );
    }


    public function testUnsortedArray(){

        $a = [
            'a'=>'a',
            'b'=>'b',
        ];
        $b = [
            'b'=>'b',
            'a'=>'a'
        ];

        $this->compare($a,$b);


        $this->is_false($a===$b);

        $this->invert();
        $this->compare($a,$b, true);

    }
    public function testComparePHPFileToString(){
        $file = dirname(__DIR__).'/input/Compare/Compare.php';
        ob_start();
        require($file);
        $target = ob_get_clean();
        if (substr($target,0,4)!='I am'){
            throw new \Exception("The source file for the test does not match /^I am.*/");
        }

        $this->compare($target,'file://'.$file);
            
        $this->compare('file://'.$file,$target);
    }
    public function testCompareFileToStringStrict(){
        $file = dirname(__DIR__).'/input/Compare/Compare.txt';
        $target = file_get_contents($file);
        if (substr($target,0,4)!='abcd'){
            throw new \Exception("The source file for the test does not match /^abcd.*/");
        }

        $this->invert();

        $this->compare($target, "file://{$file}",true);

        $this->compare("file://{$file}",$target,true);
    }

    public function testCompareFileToStringLoose(){
        $file = dirname(__DIR__).'/input/Compare/Compare.txt';
        $target = file_get_contents($file);
        if (substr($target,0,4)!='abcd'){
            throw new \Exception("The source file for the test does not match /^abcd.*/");
        }

        $this->compare($target, "file://{$file}");
        $this->compare("file://{$file}",$target);
    }
    public function testComparestringsWithDiffPad(){
        $actual = "I am a string.";
        $target = "   ".$actual;

        $this->invert();
        $this->compare($target,$actual,true);
        $this->invert();

        $this->compare($target,$actual);
    }

    public function testCompareStringsThatMatch(){
        $actual = "Be good to others.";
        $target = $actual;

        return $this->compare($target,$actual,true);
    }
}

