<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Taeluf's Development Kit  
Cli to easily initialize projects made by Taeluf.  
  
**UNDER DEVELOPMENT**  
  
## Projects Supported  
  
Available Packages:   
 - composer - composer.json file ... SET THIS UP FIRST  
 - cli - Command Line Tool  
 - scrawl - Generate documentation  
 - bigdb - Database, Orm, Migrations, Stored Queries  
  
  
  
## Documentation  
- TODO  
- Usage: `vendor/bin/tlf list` and `vendor/bin/tlf setup [package]`  
  
## Install  
```bash  
composer require taeluf/tlf v0.1.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/tlf": "v0.1.x-dev"}}  
```  
  
  
## Usage  
  
