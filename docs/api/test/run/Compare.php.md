<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Compare.php  
  
# class Tlf\Tester\Test\Compare  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testCompareLines()`   
- `public function testUnsortedArray()`   
- `public function testComparePHPFileToString()`   
- `public function testCompareFileToStringStrict()`   
- `public function testCompareFileToStringLoose()`   
- `public function testComparestringsWithDiffPad()`   
- `public function testCompareStringsThatMatch()`   
  
