<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/db/orm/Article.php  
  
# class Tlf\ProjectSample\Article  
Represents an article in the database.  
See source code at [/src/db/orm/Article.php](/src/db/orm/Article.php)  
  
## Constants  
  
## Properties  
- `public \Tlf\BigDb $db;`   
- `public int $id = null;` Placeholder to force getTags() setter.  
- `public string $uuid = null;`   
- `public string $title;`   
- `public string $slug;` should match [a-z0-9\-]  
- `public int $photo_id = null;`   
- `public int $story_id = null;`   
- `public int $author_id;`   
- `protected string $tags = '';`   
- `public \DecaturVote\News\Types\ArticleStatus $status = \DecaturVote\News\Types\ArticleStatusPublic;`   
- `public \DecaturVote\News\Types\ArticleType $type;`   
- `public bool $show_history = true;`   
- `public \DateTime $created_at;`   
- `public \DateTime $updated_at;`   
- `public string $story_slug = null;`   
- `public string $story_title = null;`   
- `protected string $body = '';` html article body  
- `protected string $url;`   
- `protected \Dv\File\Db\File $photo;`   
- `protected Story $story;`   
- `protected array $tidbits;` array<int index, DecaturVote\News\Orm\Tidbit tidbit> array of tidbits referencing this article  
- `protected array $sources;` array<int index, DecaturVote\News\Orm\Source source> array of sources referencing this article  
- `protected Author $author;`   
- `protected array $tag_list = [];`   
- `protected string $photoUrl = null;`   
  
## Methods   
- `public function __construct(\DecaturVote\News\Db\BigNewsDb $db)`   
- `public function set_from_db(array $row)`   
- `public function get_db_row(): array`   
- `public function set_from_form(array $data, mixed $form_id = null)`   
- `public function onWillSave(array $row): array` Prepare for save:  
- Set story_title & story_slug if story_id is set  
- Set own slug (if not already set)  
  
Notes:  
- does NOT set author_id, that's done during set_from_form. If not using set_from_form, it's the programmer's responsibility to determine & set author_id.  
  
- `public function onDidSave(array $row)` Set the UUID, store body to disk, run search integration, run diff integration  
TODO: Run mentions integration  
- `public function onDidDelete(array $row)`   
- `public function getTags(): array` Get array of tags. Lazy-loads from db.  
  
- `public function getHtmlBody()`   
- `public function getBody(): string` Get the article body. Lazy loads from disk.  
  
- `public function setBody(string $body)`   
- `public function getUrl()`   
- `public function setBreadcrumbs()` Set page breadcrumbs for the article, by invoking the News integration.  
- `public function getStory(): \DecaturVote\News\Orm\Story`   
- `public function getTidbits(): array` Get an array of tidbits  
- `public function getSources(): array` Get an array of tidbits  
- `public function getStoryUrl(): string` Get url of the story this article is about  
- `public function getStoryTitle(): string` Get title of the story this article is about  
- `public function getEditUrl()`   
- `public function getReadableType()` Get a human friendly type string.  
- `public function getTypeLink(): string` Get a url to the type's description  
- `public function update_search(): bool` Add this article to the search database ONLY IF its status is public. Delete it from search if not public.  
  
  
