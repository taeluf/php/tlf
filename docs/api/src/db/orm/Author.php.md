<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/db/orm/Author.php  
  
# class Tlf\ProjectSample\Author  
  
See source code at [/src/db/orm/Author.php](/src/db/orm/Author.php)  
  
## Constants  
  
## Properties  
- `public int $id = null;`   
- `public string $uuid = null;`   
- `public string $name;`   
- `public string $slug;` should match [a-z0-9\-]  
- `public string $about;`   
- `public int $photo_id = null;`   
- `public int $user_id = null;`   
- `public \DateTime $created_at;`   
- `public \DateTime $updated_at;`   
- `protected \Dv\File\Db\File $photo;`   
- `protected string $previous_about_section = '';` set during onWillSave() & used during onDidSave() to handle diffs updates  
  
## Methods   
- `public function getUrl()`   
- `public function __construct(\DecaturVote\News\Db\BigNewsDb $db)`   
- `public function set_from_db(array $row)`   
- `public function get_db_row(): array`   
- `public function set_from_form(array $data, mixed $form_id = null)`   
- `public function onWillSave(array $row): array` Prepare for save:  
- Set story_title & story_slug if story_id is set  
- Set own slug (if not already set)  
  
Notes:  
- does NOT set author_id, that's done during set_from_form. If not using set_from_form, it's the programmer's responsibility to determine & set author_id.  
  
- `public function onDidSave(array $row)` Set the UUID, store body to disk, run search integration, run diff integration  
TODO: Run mentions integration  
- `public function onDidDelete(array $row)`   
- `public function update_search(): bool` Add this author to the search database  
  
  
