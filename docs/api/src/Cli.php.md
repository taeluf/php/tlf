<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Cli.php  
  
# class Tlf\Easy\Cli  
  
See source code at [/src/Cli.php](/src/Cli.php)  
  
## Constants  
  
## Properties  
- `protected array $commands = [];`   
  
## Methods   
- `public function init()`   
- `public function exec_command(\Tlf\Cli $cli, array $args)` Execute the currently called command  
  
  
