<?php
/**
 * Deliver a Liaison website
 *
 */

require(dirname(__DIR__,2).'/vendor/autoload.php');

$lia = new \Tlf\ProjectSample\Lia();

$app = new \Tlf\ProjectSample\App($lia, 'projectsample:main', __DIR__, '/');
$lia->addApp($app);

$lia->deliver();
