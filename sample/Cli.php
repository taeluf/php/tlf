<?php

namespace Tlf\ProjectSample;

/**
 * Command Line class
 *
 * To add a command:
 * 1. add it to the $commands array, to add it to the help menu
 * 2. Create a method `cmd_command_name(array $unnamed_args, array $named_args){}`
 */
class Cli extends \Tlf\Easy\Cli {

    /**
     * Commands to display in the help menu. 
     * Each command must have a corresponding method.
     *
     * Commands like `create-app` will use invoke method `cmd_create_app(...)`
     */
    protected array $commands = [
        'setup'=> 'Setup a sample package in the current dir. Usage: tlf setup [package]',
        'list' => 'List sample packages available for setup. Usage: tlf list',
    ];

    /**
     * Get built-in packages available for setup
     * @return array identical to the packages.json file
     */
    public function get_packages(): array {
        $path = dirname(__DIR__).'/src/packages.json';

        return json_decode(file_get_contents($path), true);
    }

    /**
     * Setup a sample package in the current dir. 
     *
     * @usage bin/tlf setup [package]
     */
    public function cmd_setup(array $unnamed_args, array $named_args){
        $packages = $this->get_packages();
        $package_name = $unnamed_args[0] ?? null;
        if ($package_name === null){
            $this->notice("No Package", "Package name not supplied. Try: tlf setup [package_name]");
            $this->notice("List Packages", "List packages with: tlf list");
            echo "\n\n";
            return;
        } else if (!isset($packages[$package_name])){
            $this->notice("Package Not Found", "Package '$package_name' does not exist.");
            $this->notice("List Packages", "List packages with: tlf list");
            echo "\n\n";
            return;
        }
        $package = $packages[$package_name];
        $package_files = $package['files'] ?? [];
        $package_executables = $package['executable'] ?? [];
        $package_requires = $package['require'] ?? [];
        $package_dev_requires = $package['require-dev'] ?? [];
        $package_notices = $package['notice'] ?? [];


        if (count($package_requires) > 0) {
            echo "\nThe following packages are required: \n";
            echo "\n".json_encode($package_requires,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES)."\n\n";

            if ($this->ask("Run composer require")){
                //echo "\n\n\nCOMPOSER REQUIRE \n\n\n";
                system("composer config minimum-stability dev");
                foreach ($package_requires as $package => $version){
                    system("composer require \"$package\" \"$version\"");
                }
            }
        }

        if (count($package_dev_requires) > 0) {
            echo "\nThe following packages are required in the development environment: \n";
            echo "\n".json_encode($package_dev_requires,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES)."\n\n";

            if ($this->ask("Run composer require --dev ")){
                //echo "\n\n\nCOMPOSER REQUIRE \n\n\n";
                system("composer config minimum-stability dev");
                foreach ($package_dev_requires as $package => $version){
                    system("composer require --dev \"$package\" \"$version\"");
                }
            }
        }

        $this->notice("Output Dir", "All files will be output to ".$this->pwd);
        echo "\n\nDefault Files to Create:";
        foreach ($package_files as $input_file => $output_file){
            echo "\n - $output_file";
        }
        echo "\n\n";
        $create_files = $this->prompt("Create files? (y - use defaults, n - no, c - configure)");
        $create_files = trim($create_files);
        if ($create_files === 'y'){
            $use_defaults = true;
        }
        else if ($create_files === 'c'){
            $use_defaults = false;
        }
        else if ($create_files === 'n'){
            echo "\n"; return;
        }
        else {
            $this->notice("Invalid Answer","You must answer 'y', 'c', or 'n'");
            echo "\n\n";
            return;
        }

        $actual_outputs = [];
        if ($use_defaults)$actual_outputs = $package_files;
        else {
            foreach ($package_files as $input_file => $output_file){
                $answer = $this->prompt("Write '$output_file' to ([blank] default, n to skip)");
                $answer = trim($answer);
                if ($answer==='n')continue;
                else if ($answer==='')$actual_outputs[$input_file] = $output_file;
                else {
                    $actual_outputs[$input_file] = $answer;
                }
            }
        }

        foreach ($actual_outputs as $input_file => $output_file){
            $input_path = dirname(__DIR__).'/'.$input_file;
            $output_path = $this->pwd.'/'.$output_file;

            if (file_exists($output_path)){
                $overwrite = $this->ask("File '$output_file' exists. Overwrite it");
                if (!$overwrite)continue;
            }

            if (!is_dir(dirname($output_path))){
                mkdir(dirname($output_path), 0754, true);
            }

            copy($input_path, $output_path);


            if (in_array($input_file,$package_executables)){
                $existing_perms = fileperms($output_path);
                //$executable_perms = $existing_permissions | 0b110;
                $new_permissions = ($existing_perms & 0777) | 0110;
                chmod($output_path, $new_permissions);
            }
        }


        if (count($package_notices) > 0){
            echo "\n\nNOTICES: ";
        }
        foreach ($package_notices as $index=>$notice){
            echo "\n - $notice";
        }
        echo "\n\n";

    }

    /**
     * List sample packages available for setup.
     *
     * @usage bin/tlf list
     */
    public function cmd_list(array $unnamed_args, array $named_args){
        $packages = $this->get_packages();
        echo "\nAvailable Packages: ";
        foreach ($packages as $package_name => $package_settings){
            $description = $package_settings['description'] ?? '';
            echo "\n - $package_name";
            if ($description!='')echo " - $description";
        }
        echo "\n";
    }
}
